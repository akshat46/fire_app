const fs = require('fs');
const parse = require('csv-parse/lib/sync');
const Promise = require('bluebird');
const sqlite3 = Promise.promisifyAll(require('sqlite3'));
const _async = require('asyncawait/async');
const _await = require('asyncawait/await');

exports.GTFS = class {
  constructor() {
    function tableColumns(properties) {
      return properties
      .map(prop => {
        return `${prop.name} ${prop.type}`;
      })
      .join(',');
    }

    function tablePlaceholders(properties) {
      return properties
      .map(() => {
        return '?';
      })
      .join(',');
    }

    function insertRecord(stmt, record, properties) {
      stmt.run(
        properties.map(prop => {
          return record[prop.name];
        })
      );
    }

    function load(db, filename, properties) {
      const createStmt = `CREATE TABLE ${filename} (
        ${tableColumns(properties)}
      )`;
      db.run(createStmt);
      const csv = fs.readFileSync(`${__dirname}/${filename}.csv`);
      const records = parse(csv, {columns: true});
      const insertStmt = `INSERT INTO ${filename}
      VALUES (${tablePlaceholders(properties)})`;
      const stmt = db.prepare(insertStmt);
      records.forEach(record => {
        insertRecord(stmt, record, properties);
      });
      stmt.finalize();
    }

    function loadFirefighters(db) {
      load(db, 'firefighters', [
        {name: 'firefighter_id', type: 'INTEGER'},
        {name: 'group_id', type: 'INTEGER'},
        {name: 'lat', type: 'REAL'},
        {name: 'lng', type: 'REAL'},
        {name: 'name', type: 'TEXT'},
        {name: 'task', type: 'TEXT'},
        {name: 'heartrate', type: 'INTEGER'},
        {name: 'temperature', type: 'INTEGER'},
        {name: 'co_level', type: 'INTEGER'},
      ]);
    }

    this.db = new sqlite3.Database(':memory:');
    this.db.serialize(() => {
      loadFirefighters(this.db);
    });
  }

  getFirefightersByGroupId(group_id) {
    return this.db.allAsync(
      'SELECT * FROM firefighters WHERE group_id = $group_id',
      {$group_id: group_id}
    );
  }

  getAll() {
    return this.db.allAsync('SELECT * FROM firefighters');
  }

  getGroups() {
    return this.db.allAsync('SELECT DISTINCT group_id FROM firefighters');
  }
};
