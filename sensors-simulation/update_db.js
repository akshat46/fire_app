/* eslint-disable unknown-require */
const moment = require('moment');
const _async = require('asyncawait/async');
const _await = require('asyncawait/await');

const DATE_FORMAT = 'YYYYMMDD HH:mm:ss';

exports.Controller = class {
  constructor(timeRef, firefighterRef, groupsRef, data) {
    this.timeRef = timeRef;
    this.ffRef = firefighterRef;
    this.gRef = groupsRef;
    this.data = data;

    this.initialPublish();
    this.initialPublishGroups();

    this.timeRef.on(
      'value',
      snapshot => {
        _async(() => {
          const now = moment.utc(snapshot.val().moment);
          //this.publishTimeTables(now); TODO: replace with updateDB funciton
        })().catch(err => {
          console.error(err);
        });
      },
      errorObject => {
        console.error('The read failed: ' + errorObject.code);
      }
    );
  }

  async initialPublish(){
    let firefighters = await this.firefightersAll();
    var i = 1;
    for (var x of firefighters){
      let j = x.firefighter_id.toString();
      this.ffRef.child(j).set(x);
    }
  }

  async initialPublishGroups(){
    let groups = await this.firefighterGroups();
    for(var group of groups){
      var ffs = await this.firefightersByGroup(group.group_id);
      var ar = [];
      for (var firefighter of ffs){
        ar.push(firefighter.firefighter_id);
      }
      let i = group.group_id.toString();
      console.log(ar);
      this.gRef.child(i).set(ar);
    }
  }

  firefightersAll() {
    let firefighter = this.data.getAll();
    return firefighter;
  }

  firefighterGroups() {
    let groups = this.data.getGroups();
    return groups;
  }

  firefightersByGroup(group_id) {
    let firefighter = this.data.getFirefightersByGroupId(group_id);
    return firefighter;
  }
};
