const admin = require('firebase-admin');
const serviceAccount = require('./serviceAccountKey.json');
const trackerConfig = require('./tracker_configuration.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: trackerConfig.databaseURL
});

// Database references
const timeRef = admin.database().ref('current-time');
const firefighterRef = admin.database().ref('firefighters');
const groupsRef = admin.database().ref('groups');

// Library classes
const {HeartBeat} = require('./heart_beat.js');
const {GTFS} = require('./parse_data.js');
const {Controller} = require('./update_db.js');

const local_data = new GTFS();
new HeartBeat(timeRef, trackerConfig.simulation);
new Controller(timeRef, firefighterRef, groupsRef, local_data);
