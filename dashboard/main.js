// Controller file

const templates = require("./public");
// const _async = require('asyncawait/async');
// const _await = require('asyncawait/await');

var registeredUsers = [];
var data;

module.exports.root = function(req, res, next){
  var context = {
    name: "Frank",
    count: 30,
    colors: ["red", "green", "blue"]
  };
  //console.log('Cookies: ', req.cookies);
  res.render('index', context);
}

module.exports.getAllUsers = function(req, res, next){
  var firefighters = req.app.get('firefighters');
  var data;
  firefighters.on('value',
  snapshot => {
    data = snapshot.val();
    console.log("data is = " +data);
    res.json(data);
  });
}
