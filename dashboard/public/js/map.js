mapboxgl.accessToken = 'pk.eyJ1IjoiYWtzaGF0NDYiLCJhIjoiY2ptbnY5N2RnMHh4MzNwbnkyaDQ0Nmx6dCJ9.O1wq_YvpFSvvxUsun8-_EQ';

var map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/light-v9',
  center: [-121.8863, 37.3382],
  zoom: 13
});

function drawMarkers(){
  console.log("here2 " + users[0].coordinates);
  users.forEach(function(user) {
    var el = document.createElement('div');
    el.className = 'marker';
    el.setAttribute("id", user.id);
    new mapboxgl.Marker(el).setLngLat(user.coordinates).addTo(map);
  });
}

function drawFirePollygon(){
  console.log("\there2 " + fire_coordinates[0][3]);
  console.log("HEre now");
  var coord = [];
  //How many latitudes there are (longitudes will be the same amount)
  var lat_lon_length = fire_coordinates[0].length;
  var i = 0;
  var temp;
  console.log(lat_lon_length);

  while(i < lat_lon_length){
      temp = [fire_coordinates[1][i], fire_coordinates[0][i]];
      console.log(temp);
      coord.push(temp);
      i = i + 1;
  }
  //Final value is the first value (connecting the loop)
  temp = [fire_coordinates[1][0], fire_coordinates[0][0]];
  coord.push(temp);

  var source_name = "fire-area";
  var fire_polly_dict = {
      "type": "geojson",
      "data": {
          "type": "FeatureCollection",
          "features": [{
              "type": "Feature",
              "geometry": {
                  "type": "Polygon",
                  "coordinates": [
                      coord
                  ]
              }
          }]
      }
  }
  console.log(coord);
  map.on("load", function() {
      map.addSource(source_name, fire_polly_dict)

      console.log("Here after");
      var fill_fire_area = {
          "id": "fire_boundary",
          "type": "fill",
          "source": source_name,
          "paint": {
              "fill-color": "#E63E2C",
              "fill-opacity": 0.4
          },
          "filter": ["==", "$type", "Polygon"]
      }
      map.addLayer(fill_fire_area);
  });
}
var distanceContainer = document.getElementById('distance');

// GeoJSON object to hold our measurement features
var geojson = {
  "type": "FeatureCollection",
  "features": []
};

// Used to draw a line between points
var linestring = {
  "type": "Feature",
  "geometry": {
    "type": "LineString",
    "coordinates": []
  }
};

var first = true;

var cd = function(e){
  var features = map.queryRenderedFeatures(e.point, { layers: ['measure-points'] });

  // Remove the linestring from the group
  // So we can redraw it based on the points collection
  if (geojson.features.length > 1) geojson.features.pop();

  // Clear the Distance container to populate it with a new value
  distanceContainer.innerHTML = '';

  // If a feature was clicked, remove it from the map
  if (features.length) {
    var id = features[0].properties.id;
    geojson.features = geojson.features.filter(function(point) {
      return point.properties.id !== id;
    });
  } else {
    var point = {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          e.lngLat.lng,
          e.lngLat.lat
        ]
      },
      "properties": {
        "id": String(new Date().getTime())
      }
    };

    geojson.features.push(point);
  }

  if (geojson.features.length > 1) {
    linestring.geometry.coordinates = geojson.features.map(function(point) {
      return point.geometry.coordinates;
    });

    geojson.features.push(linestring);

    // Populate the distanceContainer with total distance
    var value = document.createElement('pre');
    value.textContent = turf.lineDistance(linestring).toLocaleString();
    distanceContainer.appendChild(value);
  }

  map.getSource('geojson').setData(geojson);
}

function distanceDrawer(){

    map.addSource('geojson', {
      "type": "geojson",
      "data": geojson
    });
    // Add styles to the map
    map.addLayer({
      id: 'measure-points',
      type: 'circle',
      source: 'geojson',
      paint: {
        'circle-radius': 5,
        'circle-color': '#000'
      },
      filter: ['in', '$type', 'Point']
    });
    map.addLayer({
      id: 'measure-lines',
      type: 'line',
      source: 'geojson',
      layout: {
        'line-cap': 'round',
        'line-join': 'round'
      },
      paint: {
        'line-color': '#000',
        'line-width': 2.5
      },
      filter: ['in', '$type', 'LineString']
    });

  map.on('click', cd);
}



function removeDistanceDrawer(){
  map.off('click', cd);
  map.removeLayer('measure-points');
  map.removeLayer('measure-lines');
  map.removeSource('geojson');
  setTimeout(function(){
    $("#distance").empty();
    $("#distance").html("...");
  },600);
  geojson.features = [];
  console.log("geojson " +JSON.stringify(geojson));
}
