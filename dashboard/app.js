// Main App File
const admin = require('firebase-admin');
const serviceAccount = require('./serviceAccountKey.json');
const trackerConfig = require('./tracker_configuration.json');
const express = require('express');
const routes = require("./router");
var path = require('path');

const app = express();
const port = 3000;

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: trackerConfig.databaseURL
});

const timeRef = admin.database().ref('current-time');
const firefighterRef = admin.database().ref('firefighters');
const groupsRef = admin.database().ref('groups');

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/public/templates'));
app.use(express.static(__dirname + '/public'));

// database references as app variables
app.set('firefighters', firefighterRef);
app.set('groups', groupsRef);

app.listen(port, () => console.log('app listening on port 3000.'));

app.use('/', routes);
