# I-Care


### Sensors-simulation: 
Sensor end node application that currently uses a .csv file to populate firebase. It can later on be integrated with actual sensors. 

### Dashboard 
Front end node application derived from django SmokeJumper. Implemented with node, express, and ejs. 

---

#### Run Application 

Both the applications will run with the following commands in their respective directories: 

```sh
$ npm install
$ npm run main 
# or 
$ node app.js
```

Dashboard includes a gulpfile and so can also be run with 

```sh
$ gulp
# or 
$ npx gulp
```

**Note:** 3.9.1 gulp is required. (latest is 4.0.0)
